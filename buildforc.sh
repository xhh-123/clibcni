# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# shellcheck disable=SC2154,SC2034

set -x
set -e

export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib/x86_64-linux-gnu/:$LD_LIBRARY_PATH


BUILD_DIR=/tmp/build_clibcni

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
# lcr
cd $BUILD_DIR
git clone https://gitee.com/openeuler/lcr.git

# clibcni
git clone https://gitee.com/openMajun/clibcni.git

mkdir -p build
cd build
cmake -DDEBUG=ON -DENABLE_UT=ON ../
make_build